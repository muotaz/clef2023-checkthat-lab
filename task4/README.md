# Task 4: Factuality of Reporting of News Media




## Data-sharing Agreement

**Data is available at**



__Table of contents:__
TBA
<!-- - [Evaluation Results](#evaluation-results)
- [List of Versions](#list-of-versions)
- [Contents of the Task 3 Directory](#contents-of-the-repository)
- [Input Data Format](#input-data-format)
	- [Task 3: Multi-Class Fake News Detection of News Articles](#Subtask-3A-Multi-Class-Fake-News-Detection-of-News-Articles)
- [Output Data Format](#output-data-format)
	- [Task 3: Multi-Class Fake News Detection of News Articles](#Multi-Class-Fake-News-Detection-of-News-Articles)
- [Format Checkers](#format-checkers)
	- [Task 3: Multi-Class Fake News Detection of News Articles](#Subtask-3A-Multi-Class-Fake-News-Detection-of-News-Articles-2)
- [Scorers](#scorers)
	- [Task 3: Multi-Class Fake News Detection of News Articles](#Subtask-3A-Multi-Class-Fake-News-Detection-of-News-Articles-3)
- [Evaluation Metrics](#evaluation-metrics)
- [Baselines](#baselines)
	- [Task 3: Multi-Class Fake News Detection of News Articles](#Subtask-3A-Multi-Class-Fake-News-Detection-of-News-Articles-4)
- [Credits](#credits)
 -->
## Evaluation Results

TBA

## List of Versions
TBA
<!-- - **Task 3--English-v1.0 [2022/24/03]** - data for task 3 is released. -->

## Contents of the Task 3 Directory
TBA

<!-- We provide the following files:

- Main folder: [data](./data)
  - subfolder: Task 3--english
- Main folder: [baseline](./baseline)<br/>
- 	Contains scripts provided for baseline models of the tasks
- Main folder: [evaluation](./format_checker)<br/>
- 	Contains scripts provided to evaluate submission file
- [README.md](./README.md) <br/>
- 	This file! -->



# Input Data Format

<!-- The data will be provided in the format of Id, title, text, rating, domain the description of column are as follows: -->

TBA

# Output Data Format

TBA


# Format Checkers

TBA


# Evaluation Metrics
TBA
<!-- 
This task is evaluated as a classification task. We will use the F1-macro measure for the ranking of teams. There is a limit of 5 runs (total and not per day), and only one person from a team is allowed to submit runs.

Submission Link: Coming Soon

Evaluation File task3/evaluation/CLEF_-_CheckThat__Task3ab_-_Evaluation.txt -->

# Baselines

<!-- ### Task 3: Multi-Class Fake News Detection of News Articles

For this task, we have created a baseline system. The baseline system can be found at https://zenodo.org/record/6362498
 -->
## Scorers


## Credits
Please find it on the task website: https://checkthat.gitlab.io/clef2023/task4/