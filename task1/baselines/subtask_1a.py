import jsonlines
import json
import random
import logging
import argparse
from os.path import join, dirname, basename
from sklearn.dummy import DummyClassifier
from sklearn.decomposition import PCA
from sklearn.svm import SVC
import numpy as np
import os

import sys
sys.path.append('.')

from scorer.subtask_1 import evaluate
from format_checker.subtask_1 import check_format

random.seed(0)
ROOT_DIR = dirname(dirname(__file__))

logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

def run_majority_baseline(train_fpath, test_fpath, results_fpath):
    train_objs = [obj for obj in jsonlines.open(train_fpath)]
    test_objs = [obj for obj in jsonlines.open(test_fpath)]

    pipeline = DummyClassifier(strategy="most_frequent")
    pipeline.fit([obj["tweet_text"] for obj in train_objs], [obj["class_label"] for obj in train_objs])

    with open(results_fpath, "w") as results_file:
        predicted_distance = pipeline.predict([obj["tweet_text"] for obj in test_objs])

        results_file.write("tweet_id\tclass_label\trun_id\n")

        for i, line in enumerate(test_objs):
            label = predicted_distance[i]

            results_file.write("{}\t{}\t{}\n".format(line["tweet_id"], label, "majority"))


def run_random_baseline(data_fpath, results_fpath):
    gold_objs = [obj for obj in jsonlines.open(data_fpath)]
    label_list= [obj["class_label"] for obj in gold_objs]

    with open(results_fpath, "w") as results_file:
        results_file.write("tweet_id\tclass_label\trun_id\n")
        for i, line in enumerate(gold_objs):
            results_file.write('{}\t{}\t{}\n'.format(line["tweet_id"],random.choice(label_list), "random"))


def run_imgbert_baseline(data_dir, split, train_fpath, test_fpath, results_fpath):
    tr_feats = json.load(open(os.path.join(data_dir, "train_feats.json")))
    te_feats = json.load(open(os.path.join(data_dir, "%s_feats.json"%(split))))
    train_id_lab = [[obj["tweet_id"], obj["class_label"]] for obj in jsonlines.open(train_fpath)]
    test_id_lab = [[obj["tweet_id"], obj["class_label"]] for obj in jsonlines.open(test_fpath)]

    tr_cat_feats = [tr_feats["imgfeats"][obj[0]]+tr_feats["textfeats"][obj[0]] for obj in train_id_lab]
    tr_cat_feats = np.array(tr_cat_feats)
    te_cat_feats = [te_feats["imgfeats"][obj[0]]+te_feats["textfeats"][obj[0]] for obj in test_id_lab]
    te_cat_feats = np.array(te_cat_feats)

    clf = SVC(C=1, kernel='linear', random_state=0)
    clf.fit(tr_cat_feats, [obj[1] for obj in train_id_lab])

    with open(results_fpath, "w") as results_file:
        predicted_distance = clf.predict(te_cat_feats)
        results_file.write("tweet_id\tclass_label\trun_id\n")

        for i, line in enumerate(test_id_lab):
            label = predicted_distance[i]

            results_file.write("{}\t{}\t{}\n".format(line[0], label, "imgbert"))



def run_baselines(data_dir, test_split, train_fpath, test_fpath):
    ## Write test file in format
    test_objs = [obj for obj in jsonlines.open(test_fpath)]
    gold_fpath = join(ROOT_DIR, f'data/gold_{basename(test_fpath.replace("jsonl", "txt"))}')
    with open(gold_fpath, "w") as gold_file:
        gold_file.write("tweet_id\ttweet_url\ttweet_text\tcheck_worthiness\n")
        for i, line in enumerate(test_objs):
            gold_file.write('{}\t{}\t{}\t{}\n'.format(line["tweet_id"], line["tweet_url"], " ".join(line["tweet_text"].split()), line["class_label"]))

    majority_baseline_fpath = join(ROOT_DIR,
                                 f'data/majority_baseline_{basename(test_fpath.replace("jsonl", "txt"))}')
    run_majority_baseline(train_fpath, test_fpath, majority_baseline_fpath)

    if check_format(majority_baseline_fpath):
        acc, precision, recall, f1 = evaluate(gold_fpath, majority_baseline_fpath)
        logging.info(f"Majority Baseline for English F1 (positive class): {f1}")


    random_baseline_fpath = join(ROOT_DIR, f'data/random_baseline_{basename(test_fpath.replace("jsonl", "txt"))}')
    run_random_baseline(test_fpath, random_baseline_fpath)

    if check_format(random_baseline_fpath):
        acc, precision, recall, f1 = evaluate(gold_fpath, random_baseline_fpath)
        logging.info(f"Random Baseline for English F1 (positive class): {f1}")


    imgbert_baseline_fpath = join(ROOT_DIR, f'data/imgbert_baseline_{basename(test_fpath.replace("jsonl", "txt"))}')
    run_imgbert_baseline(data_dir, test_split, train_fpath, test_fpath, imgbert_baseline_fpath)

    if check_format(imgbert_baseline_fpath):
        acc, precision, recall, f1 = evaluate(gold_fpath, imgbert_baseline_fpath)
        logging.info(f"ImgBert Baseline for English F1 (positive class): {f1}")



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_dir", "-r", required=True, type=str,
                        help="The absolute path to the features")
    parser.add_argument("--test_split", "-s", required=True, type=str,
                        default="dev", help="Test split name")
    parser.add_argument("--train-file-path", "-t", required=True, type=str,
                        help="The absolute path to the training data")
    parser.add_argument("--dev-file-path", "-d", required=True, type=str,
                        help="The absolute path to the dev data")

    args = parser.parse_args()
    run_baselines(args.data_dir, args.test_split, args.train_file_path, args.dev_file_path)